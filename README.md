# email-collapser

Collapse a hierarchy of folders in Import folder from an IMAP inbox to a single
top level folder.

e.g. from this:

```
Import
├── import1
│   └── email1
├── import2
│   └── email2
└── import3
    └── import4
        └── email3
```

To this:

```
Import
├── email1
├── email2
└── email3
```

# How do I use

Have python3 and pip

```bash
$ pip3 install imap-tools
$ ./collapser.py --email "youremail@example.com" --password "yourpassword" --server "imap.yourserver.example.com"
# NOTE this runs in dry run mode, run with --execute when you're ready for it to
# actually move emails and delete empty folders!
```

Now you're done. Note that this may take a while if you have a lot of emails and
folders.

# Motivation

There are a lot of reasons why you might want to use this.

The reason I used it though is that my old email provider only let me export my
emails using .eml files. So I imported them all using Apple Mail and they
imported in an insane tree structure with timestamps as when they were created.

I don't like that. So I made this.

# ur the imap king

u no it
