#!/usr/bin/env python3

from typing import List
from imap_tools import MailBox
import argparse
from datetime import datetime

DESTINATION_FOLDER = "Import"


def to_uid(msg):
    return msg.uid


def move_emails_to_import(mailbox: MailBox, folder: str, execute: bool):
    """
    Moves email uids to raw import folder
    """
    if execute:
        mailbox.folder.set(folder.name)
        uids = mailbox.uids()
        mailbox.move(uids, DESTINATION_FOLDER)
        print(
            f"{datetime.now().timestamp()} Successfully moved emails from folder: {folder.name} to folder: {DESTINATION_FOLDER}"
        )
    else:
        print(
            f"{datetime.now().timestamp()} Would have moved uids emails in folder: {folder} emails to folder: {DESTINATION_FOLDER}"
        )


def delete_folder(mailbox: MailBox, folder: str, execute: bool):
    """
    Deletes empty folder
    """

    up_folder = folder.upper()
    if up_folder == "INBOX" or up_folder == "ARCHIVE" or up_folder == "IMPORT":
        # can't hurt to be safe
        return

    if execute:
        mailbox.folder.set(folder)
        if mailbox.uids():
            print(
                f"{datetime.now().timestamp()} Skipping deletion of nonempty folder: {folder}"
            )
            return
        print(
            f"{datetime.now().timestamp()} Deleting old folder {folder} now that import is complete"
        )
        try:
            mailbox.folder.delete(folder)
        except Exception as e:
            print(
                f"{datetime.now().timestamp()} Folder: {folder} does not exist. Cannot delete: {e}"
            )
    else:
        print(f"{datetime.now().timestamp()} Would have deleted folder: {folder}")


def run_on_mailbox(email: str, password: str, server: str, execute: bool):
    """
    Executes collapsing function on mailbox with given credentials.
    """
    with MailBox(server).login(email, password) as mailbox:
        folders = []
        for folder in mailbox.folder.list(DESTINATION_FOLDER):
            if folder.name == DESTINATION_FOLDER:
                continue
            folders.append(folder)

        for folder in folders:
            move_emails_to_import(mailbox, folder, execute)

    print(
        f"{datetime.now().timestamp()} Getting data for deletion. This may take a moment..."
    )
    with MailBox(server).login(email, password) as mailbox:
        folders = []
        for folder in mailbox.folder.list(DESTINATION_FOLDER):
            folders.append(folder.name)

        for folder in folders:
            delete_folder(mailbox, folder, execute)


def main():
    parser = argparse.ArgumentParser(
        description=f"Flatten all mail folders in {DESTINATION_FOLDER} folder to be single list of emails in {DESTINATION_FOLDER} and delete empty folders after"
    )
    parser.add_argument(
        "--execute",
        "-e",
        dest="execute",
        help="Actually move emails (runs in dry run mode otherwise)",
        action="store_true",
    )
    parser.add_argument(
        "--email", dest="email", help="Email address to work on", required=True
    )
    parser.add_argument(
        "--password",
        dest="password",
        help="User password or generated app specific password for email",
        required=True,
    )
    parser.add_argument(
        "--server",
        dest="server",
        help="IMAP server address (requires TLS or SSL and port 993)",
        required=True,
    )
    args = parser.parse_args()
    email = args.email
    password = args.password
    execute = args.execute
    server = args.server
    run_on_mailbox(email, password, server, execute)


if __name__ == "__main__":
    main()
